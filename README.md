# Git Exercise

A dummy project to get familiar with Git Architecture.

Contributors Need to follow these steps
- clone the repository to your local machine
- create a new branch with name 'feature-1'
- checkout 'feature-1' branch
- add your name and email id in the Contributors list below
- commit the changes in your local repository
- push the updates to the remote repository
- raise a pull request to get it merged with the master branch
- update the file [UNIX-Commands.md](./UNIX-Commands.md)


# Contributors

1. Prince M S - prince.mi3@iiitmk.ac.in
2. Sachin V S - sachin.mi20@iiitmk.ac.in -Machine Intelligence
3. Shahal K P - shahal.mi20@iiitmk.ac.in - Machine Intelligence
4. Ragesh K R - ragesh.mi20@iiitmk.ac.in - Machine Intelligence
5. Simran Kaur - skaur6526@gmail.com - Data Analytics
6. Murali Krishna - murali.da20@iiitmk.ac.in
7. Neema Abdulkader - neema.cs20@iiitmk.ac.in - Cyber Security
8. Jeeva Thankachan - jeevathankj01@gmail.com - Cyber Security
9. Rohaan George N - rohaangeorgen1@gmail.com - Machine Intelligence
10. Sarin Jickson - sarin.mi20@iiitmk.ac.in - Machine Intelligence
11. Vishnu K - vishnudinesh2011@gmail.com - Geospatial Analytics
12. Gayathri V - gayathrivijayan99@gmail.com - Data Analytics
13. Anit Anna Joseph - anitannajoseph@gmail.com - Data Analytics
14. Gayathri Prasad B- gayathribindumn1999@gmail.com
15. Justin Joseph - justinjoseph.mi20@iiitmk.ac.in - Machine Intelligence
16. Palle Lakshmi Parimala - parimala.palle07@gmail.com - Data Analytics
17. Mukut Chakraborty - mukutchk110@gmail.com
18. Manju G K - manju.mi20@iiitmk.ac.in - machine intelligence
19. Christeena Mary Thomas - christeeena.mi20@iiitmk.ac.in -Machine intelligence
20. Gopika G P - gopika.mi20@iiitmk.ac.in - Machine Intelligence 
21. Ravi Prakash - 1907prakash.ravi@gmail.com - Cyber Security
22. Jyothi Lakshmi K - jyothi.mi20@iiitmk.ac.in - Machine Intelligence
23. A Umashankar Rao - umashankar.da20@iiitmk.ac.in - Data Analytics
24. Somalini Panda - somalini.panda@gmail.com - Machine Intelligence
25. Mayukha P - mayukha.ga20@iiitmk.ac.in - Geospatial Analytics
26. Row Ashritha - ashritha.da20@iiitmk.ac.in - Data Analytics
27. Prajwal Nimje - nimjeprajwal1997@gmail.com - Data Analytics
28. Christy M Thomas - christymthmas@gmail.com	- Geospatial Analytics
