# Pre-requisites for Linux/Unix  

In order to work on this project, all the members are required to have a grip on the basics of Linux terminal commands.  

Following are few of the most frequently accessed **Linux/Unix terminal commands** and **directories**. The idea is to make everyone well aware of them with a brief description.  

_Describe the significance of the following (commands as well as directories) in a short yet optimal manner!_  

## Terminal Commands  

* `ls`: https://www.geeksforgeeks.org/practical-applications-ls-command-linux/  
* `ll`: an alias for `ls -l` 
* `umask`: https://linuxize.com/post/umask-command-in-linux/  
* `cchmod`: <!-- Replace this block with your description -->  
* `chmod`: https://www.geeksforgeeks.org/chmod-command-linux/  
* `chown`: https://www.geeksforgeeks.org/chown-command-in-linux-with-examples/  
* `chgrp`: <!-- Replace this block with your description -->  
* `mount`: <!-- Replace this block with your description -->  
* `umount`: <!-- Replace this block with your description -->  
* `strace`: <!-- Replace this block with your description -->  
* `rm`: remove/delete one or multiple files (these are removed permanently from system) [\(see manual\)](https://man.cx/rm)  
* `ps`: lists currently running processes (with process IDs and other basic info) [\(see manual\)](https://man.cx/ps)  
* `vmstat`: <!-- Replace this block with your description -->  
* `ifconfig`: https://www.geeksforgeeks.org/ifconfig-command-in-linux-with-examples/  
* `ip`: <!-- Replace this block with your description -->  
* `who`: prints the present user profile [\(see manual\)](https://man.cx/who)  
* `which`: describes the operand (location) eg. `$ which $SHELL` [\(see manual\)](https://man.cx/which)  
* `finger`: <!-- Replace this block with your description -->  
* `ssh`: used for (generally) remote access through ssh i.e. _secured shell_ [\(see manual\)](https://man.cx/ssh)  
* `ftp`: <!-- Replace this block with your description -->  
* `scp`: <!-- Replace this block with your description -->  
* `ce`: <!-- Replace this block with your description -->  
* `cd`: used to change (_c_) directory (_d_) [\(see manual\)](https://man.cx/cd)  
* `pwd`: prints the current (present) working directory [\(see manual\)](https://man.cx/pwd)  
* `grep`: frequently used to filter out and extract the specified pattern/sub-string (using RegEx) [\(see manual\)](https://man.cx/grep)  
* `sed`: <!-- Replace this block with your description -->  
* `awk`: <!-- Replace this block with your description -->  
* `nohup`: <!-- Replace this block with your description -->  
* `fg`: brings any background running process to foreground _(fg)_ [\(see manual\)](https://man.cx/fg)  
* `bg`: places any foreground process to background _(bg)_ [\(see manual\)](https://man.cx/bg)  
* `touch`: <!-- Replace this block with your description -->  
* `kill`: <!-- Replace this block with your description -->  
* `tail`: fetches from bottom (eg. can be used to print last 3 lines of a file) [\(see manual\)](https://man.cx/tail)  
* `head`: <!-- Replace this block with your description -->  
* `top`: <!-- Replace this block with your description -->  
* `vi` and `vim`: <!-- Replace this block with a COMMON description -->  
    1. `vi`: <!-- Replace this block with your description -->  
    2. `vim`: <!-- Replace this block with your description -->  
* `cat`: prints the contant of specified file on terminal [\(see manual\)](https://man.cx/cat)  
* `man`: prints the reference manual for the given cammand [\(see manual\)](https://man.cx/man)  
* `ln`: <!-- Replace this block with your description -->  
* `useradd`, `usermod` and `userdel`: used to handle the operations related to existing or new users  
    1. `useradd`: adds/creates new user to the system [\(see manual\)](https://man.cx/useradd)  
    2. `usermod`: <!-- Replace this block with your description -->  
    3. `userdel`: <!-- Replace this block with your description -->  
* `groupadd` and `groupdel`: similar to above, just works for groups  
    1. `groupadd`: <!-- Replace this block with your description -->  
    2. `groupdel`: used to remove an existing group from the system [\(see manual\)](https://man.cx/groupdel)  
* `systemctl`: <!-- Replace this block with your description -->  
* `init`: <!-- Replace this block with your description -->  
* `reboot`: kills all the running processes and shuts down the system i.e. followed by a fresh booting-up of OS [\(see manual\)](https://man.cx/reboot)  
* `restart`: exactly same as `reboot` [\(see manual\)](https://man.cx/restart)  
* `screen`: <!-- Replace this block with your description -->  

## Directories  

* `/usr/bin` and `/usr/local/bin`: <!-- Replace this block with a COMMON description -->  
    1. `/usr/bin`: consists the executable files that are managed by OS distribution  
    2. `/usr/local/bin`: <!-- Replace this block with your description -->  
* `/etc`: consists the configuration file those can be modified easily too viz. fonts etc..  
* `/var/logs`: <!-- Replace this block with your description -->  
